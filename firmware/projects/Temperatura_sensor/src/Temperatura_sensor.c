/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * * SF 	Sánchez, Franco
	LF		Lovatto, Flavio
 *
 *
 *
 * Revisión:
 * 27-10-19: Versión inicial

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "../inc/Temperatura_sensor.h"
#include "switch.h"
#include "analog_io.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"  /*La aplicacion no deberia conocer a gpio, pero como ningun driver utilizado la incluye debemos incluirla
para que funcione el toggleo de un gpio*/

/*==================[macros and definitions]=================================*/

#define ON 1		/*Banderas de estados alto y bajo*/
#define OFF 0

#define BAUDRATE 115200
#define DECIMAL 10
#define FM 20 						/*Frecuencia de muestreo ->> en 1 s tenemos 20 muestras */
#define TIEMPO_DE_ADQUISICION 1 	/*  TIEMPO QUE SE ADQUIERE LA SEÑAL EN SEGUNDOS ->> PARA CALCULAR LA CANTIDAD DE MUESTRAS*/

/* letras en codigo ASCII*/
#define A 66
#define B 67
#define C 68

/*==================[internal data definition]===============================*/

analog_input_config AD_conversor;
timer_config timmerA;
serial_config puerto_PC={SERIAL_PORT_PC,BAUDRATE,NO_INT};
serial_config puerto_RS_485={SERIAL_PORT_RS485,BAUDRATE, &Tecla};


int b=0;
uint16_t VALOR=0; 		/*Variable donde se guarda la conversion del AD*/
uint16_t TECLA=0;
uint16_t  TEMPERATURAS[20]; 	/* VECTOR QUE GUARDAD LAS PRESIONES CADA 50 ms*/
uint16_t AUX=0; 		/*VARIABLE A UTILIZAR PARA ENCONTRAR LA TEMP MAX, MINIMA Y PROM */
uint16_t MAXIMO=0; 		/*VARIABLE A GUARDAR LA MAXIMA TEMPERATURA EN EL ULTIMO  SEGUNDO*/
uint16_t MINIMO=0;		/*VARIABLE A GUARDAR LA MINIMA TEMPERATURA EN EL ULTIMO  SEGUNDO*/
uint16_t PROMEDIO=0;	/*VARIABLE A GUARDAR LA TEMPERATURA PROMEDIO EN EL ULTIMO  SEGUNDO*/
uint16_t CANT_MUESTRAS=20;	 /*DEPENDE DE LA FREC MUESTREO Y EL TIEMPO DE ADQUISICION*/
bool TECLA_APRETADA;
bool DATOS_CALCULADOS;
/*==================[internal functions declaration]=========================*/

/* funcion que opera con el vector TEMPERATURAS (global) y calcula el maximo de todas las TEMPERATURAS EN 1 SEG*/
uint16_t maximo (void)
{
		int i;
		AUX=TEMPERATURAS[0];
		for(i=1;i<CANT_MUESTRAS;i++)
		{
			if(TEMPERATURAS[i]>AUX)
				{
				AUX=TEMPERATURAS[i];
				}

		}
		return(AUX);
}

/* Funcion que opera con el vector TEMPERATURAS (global) y calcula el maximo de todas las TEMPERATURAS EN 1 SEG*/
uint16_t minimo (void)
{
		int i;
		AUX=TEMPERATURAS[0]; /* asigno a la variable AUX la primer temperatura para comparar*/

		for(i=1;i<CANT_MUESTRAS;i++)
		{
			if(TEMPERATURAS[i]<AUX)
				{
				AUX=TEMPERATURAS[i];
				}

		}
		return(AUX);
}

/* FUNCION QUE ENVIA EL PROMEDIO DE LAS TEMPERATURAS A TRAVES DE LA VARIABLE AUX*/
uint16_t PROM(void)
{
	int a;
	AUX=0;
	for(a=0;a<CANT_MUESTRAS;a++)
	{
		AUX=AUX+TEMPERATURAS[a];
	}
	AUX=AUX/CANT_MUESTRAS;
	return(AUX);
}


/*==================[external data definition]===============================*/

/*==================[external functions declaration]==========================*/

void ptr_int_timmerA(void)
{
	AnalogStartConvertion();   /*ARRANCO A CONVERTIR CADA 50 ms */
}


void ptr_int_ad_conversor(void)
{

	AnalogInputRead(CH1, &VALOR);
	TEMPERATURAS[b]=VALOR;
	b++;
	if(b==CANT_MUESTRAS)
		{
		b=0;
		MAXIMO=maximo();
		PROMEDIO=PROM();
		MINIMO=minimo();
		DATOS_CALCULADOS=ON;
		}
}

/* INTERRUPCION DEL PUERTO UART SERIE RS485 PARA SABER SI LA TECLA ESTA APRETADA */
void Tecla(void)
{
	UartReadByte(SERIAL_PORT_RS485, &TECLA);
	TECLA_APRETADA=ON;
}


int main(void)
{
	/*DECLARACIONES*/
		gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; /* Vector que configura el usuario y se lo pasa al driver display*/

		AD_conversor.input=CH1;
		AD_conversor.mode=AINPUTS_SINGLE_READ;
		AD_conversor.pAnalogInput=ptr_int_ad_conversor;

		/*INICIALIZAICIONES*/
		LedsInit();
		ITSE0803Init(pines);
		SystemClockInit();
		UartInit(&puerto_PC); /*iNICIALIZO EL PUERTO DEL CONVERSOR*/
		UartInit(&puerto_RS_485); /* INICIALIZO EL PUERTO PARA EL PASAJE EN SERIE DE LA TECLA APRETADA*/

	   timmerA.timer=TIMER_A;  /*configuramos el timmer A con un periodo de 50 ms */
	   timmerA.period=50; /* EL TIMMER INTERRUMPE CADA 50 ms -> frecuencia muestreo=20 Hz */
	   timmerA.pFunc=ptr_int_timmerA;
	   TimerInit(&timmerA);
	   TimerStart(TIMER_A); /*arrancamos el timmer A */

	   AnalogInputInit(&AD_conversor);
	   AnalogOutputInit();

    while(1)
    {
    	if((TECLA_APRETADA==ON)&&(DATOS_CALCULADOS==ON))
    	{
			if(TECLA==A)
						  {
						  UartSendString(SERIAL_PORT_PC, UartItoa(MAXIMO, DECIMAL));
						  UartSendBuffer(SERIAL_PORT_PC," °C \n\r", strlen(" °C \n\r"));
						  ITSE0803DisplayValue(MAXIMO);
						  LedOn(LED_RGB_R);
						  LedOff(LED_RGB_B);
						  LedOff(LED_RGB_G);
						  }
					if(TECLA==B)
							{
							 UartSendString(SERIAL_PORT_PC, UartItoa(MINIMO, DECIMAL));
							 UartSendBuffer(SERIAL_PORT_PC," °C \n\r", strlen(" °C \n\r"));
							 ITSE0803DisplayValue(MINIMO);
							 LedOn(LED_RGB_R);
							 LedOn(LED_RGB_G);
							 LedOff(LED_RGB_B);
							}
					if(TECLA==C)
							{
							   UartSendString(SERIAL_PORT_PC, UartItoa(PROMEDIO, DECIMAL));
							   UartSendBuffer(SERIAL_PORT_PC," °C \n\r", strlen(" °C \n\r"));
							   ITSE0803DisplayValue(PROMEDIO);
							   LedOn(LED_RGB_G);
							   LedOff(LED_RGB_R);
							   LedOff(LED_RGB_B);
							}
					TECLA_APRETADA=OFF;
					DATOS_CALCULADOS=OFF;
    	}
     }

}


/*==================[end of file]============================================*/
