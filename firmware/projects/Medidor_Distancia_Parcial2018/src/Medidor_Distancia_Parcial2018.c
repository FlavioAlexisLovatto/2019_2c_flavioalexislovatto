/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * * SF 	Sánchez, Franco
 * LF 		Lovatto, Flavio
 *
 *
 *
 *
 * Revisión:
 * 25-09-19: Versión inicial

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Medidor_Distancia_Parcial2018.h"
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define BAUDRATE 9600
#define UN_METRO 100
#define DOS_METROS 200
#define DECIMAL 10

uint16_t  VALOR=0;
timer_config timmer;
serial_config puerto={SERIAL_PORT_PC,BAUDRATE,NO_INT};

/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/
/*==================[external data definition]===============================*/

void ptr_int_display_timmer(void)
							{
	 VALOR=HcSr04ReadDistanceCentimeters();
							}

/*==================[external functions declaration]==========================*/


int main(void)
{
	/*DECLARACIONES*/



	 /*INICIALIZAICIONES*/

   HcSr04Init(T_FIL2, T_FIL3); /*​T_FIL2 para inicializar el echo y T_FIL3 para el trigger */
   SystemClockInit();
   LedsInit();
   timmer.timer=TIMER_B;
   timmer.period=1000;     /*configuramos el timmer B con un periodo de 1 s */
   timmer.pFunc=ptr_int_display_timmer;
   TimerInit(&timmer);
   TimerStart(TIMER_B); /*arrancamos el timmer b */


    while(1)
    {
    	if(VALOR<UN_METRO)
    		{
    		LedOn(LEDRGB_R);
    		}
    	if(VALOR>DOS_METROS)
    	    {
    		LedOn(LEDRGB_G);
    	    }

    	if((VALOR>UN_METRO)&&(VALOR<DOS_METROS))
    		{
    		LedOn(LEDRGB_B);
    		}
    	if(VALOR<DOS_METROS)
    	{
    		UartSendString(SERIAL_PORT_PC, UartItoa(VALOR, DECIMAL));
    		UartSendBuffer(SERIAL_PORT_PC, " CM \n\r", strlen(" CM \n\r"));
    	}
     }

}





/*==================[end of file]============================================*/
