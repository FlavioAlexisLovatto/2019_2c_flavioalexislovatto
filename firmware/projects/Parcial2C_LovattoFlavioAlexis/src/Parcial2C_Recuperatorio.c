/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 *	LF		Lovatto, Flavio
 *
 *
 *
 * Revisión:
 * 28-10-19: Versión inicial

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial2C_LovattoFlavioAlexis.h"
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "switch.h"
#include "analog_io.h"
#include "DisplayITS_E0803.h"

/*==================[macros and definitions]=================================*/

#define ON 1						/*Banderas de alto o bajo*/
#define OFF 0

#define BAUDRATE 115200     		 /*Como no se especifica la velocidad de transmición tomo por defecto esta, que concidero rapida y suficiente en esta app*/
#define DECIMAL 10

#define PRESION_MIN 0				/*Definicion de la presion minima para luego usarla en los calculos*/
#define PRESION_MAX 200 			/*Definicion de la presion maxima para luego usarla en los calculos*/
#define VALOR_MAX_BINARIO 1023       /*Utilizado a la hora de convertir la presion es 2^n y el conversor es de n=10 bits*/

#define TOPE_MINIMO 50				/*Presiones maximas y minimas de interes para comparar luego y disparar alarmas(En este caso leds) si se superan o no*/
#define TOPE_MAXIMO 150

#define CANT_MUESTRAS 250      /*DEPENDE DE LA FRECUENCIA MUESTREO 250 Hz Y EL TIEMPO DE ADQUISICION 1 SEGUNDO*/

/*==================[internal data definition]===============================*/
analog_input_config AD_conversor;
timer_config timmerA;
serial_config puerto_PC={SERIAL_PORT_PC,BAUDRATE,NO_INT};


uint8_t b=0;		/*Variable Para controlar la carga del verctor*/
uint16_t VALOR=0; 	/*Variable donde se guarda la conversion del AD*/
uint16_t  PRESION[250]; 	/* VECTOR QUE GUARDAD LAS PRESIONES CADA 4 ms*/
uint16_t AUX_PRESION=0; 		/*VARIABLE A UTILIZAR PARA ENCONTRAR LA TEMP MAX, MINIMA Y PROM */
uint16_t PRESION_MAXIMA=0; 		/*VARIABLE A GUARDAR LA MAXIMA PRESION EN EL ULTIMO  SEGUNDO*/
uint16_t PRESION_MINIMA=0;		/*VARIABLE A GUARDAR LA MINIMA PRESION EN EL ULTIMO  SEGUNDO*/
uint16_t PRESION_PROMEDIO=0;	/*VARIABLE A GUARDAR LA PRESION PROMEDIO EN EL ULTIMO SEGUNDO*/
uint8_t DATOS_CALCULADOS=0;		/*BANDERA PARA SABER SI HAY DATOS CALCULADOS O NO*/
uint8_t ESTADOTECLA1=0;			/*La tecla 1 utilizo para que me de el maximo*/
uint8_t ESTADOTECLA2=0;			/*La tecla 2 utilizo para que me de el minimo*/
uint8_t ESTADOTECLA3=0;			/*La tecla 3 utilizo para que me de el promedio*/

/*==================[internal functions declaration]=========================*/

/*Funcion que opera con el vector PRESION (global) y calcula el maximo de todas las Presiones en el ultimo segundo*/
uint16_t Presion_Maxima (void)
{
	int i;
	AUX_PRESION=PRESION[0];
	for(i=1;i<CANT_MUESTRAS;i++)
	{
		if(PRESION[i]>AUX_PRESION)
		{
			AUX_PRESION=PRESION[i];
		}
	}
	return(AUX_PRESION);
}

/* funcion que opera con el vector PRESION (global) y calcula el minimo de todas las presiones en el ultimo segundo*/
uint16_t Presion_Minima (void)
{
	int i;
	AUX_PRESION=PRESION[0];
	for(i=1;i<CANT_MUESTRAS;i++)
	{
		if(PRESION[i]<AUX_PRESION)
		{
			AUX_PRESION=PRESION[i];
		}
	}
	return(AUX_PRESION);
}

/* Funcion que me devuelve el promedio de las presiones del ultimo segundo*/
uint16_t Presion_Promedio(void)
{
	int i;
	AUX_PRESION=0;
	for(i=0;i<CANT_MUESTRAS;i++)
	{
		AUX_PRESION=AUX_PRESION+PRESION[i];
	}
	AUX_PRESION=AUX_PRESION/CANT_MUESTRAS;
	return(AUX_PRESION);
}


/*==================[external data definition]===============================*/

/*==================[external functions declaration]==========================*/


/*Interrupcion del timer*/
void ptr_int_timmerA(void)
{
	AnalogStartConvertion();   /*ARRANCO A CONVERTIR CADA 4 ms */
}

/*Interrupcion del conversor y en la misma voy llenando el vector PRESION */
void ptr_int_ad_conversor(void)
{
		AnalogInputRead(CH1, &VALOR);
		/*Convierto a valor de presion el dato leido*/
		VALOR=(PRESION_MIN+((PRESION_MAX-PRESION_MIN)*(VALOR/VALOR_MAX_BINARIO)));
		PRESION[b]=VALOR;
		b++;
		if(b==CANT_MUESTRAS)
		{
			b=0;
			PRESION_MAXIMA=Presion_Maxima(); /*Llamo la funcion que devuelve la maxima presion*/

			PRESION_MINIMA=Presion_Minima();  /*Llamo la funcion que devuelve la minima presion*/

			PRESION_PROMEDIO=Presion_Promedio();  /*Llamo la funcion que devuelve el promedio de la presion*/

			DATOS_CALCULADOS=ON;
		}
}

/*Interrupcion para la tecla 1*/
void Int_Tecla1(void)
{
	ESTADOTECLA1=!ESTADOTECLA1;
	ESTADOTECLA2=OFF;
	ESTADOTECLA3=OFF;
}

/*Interrupcion para la tecla 2*/
void Int_Tecla2(void)
 {
	ESTADOTECLA2=!ESTADOTECLA2;
	ESTADOTECLA3=OFF;
	ESTADOTECLA1=OFF;
 }

/*Interrupcion para la tecla 3*/
void Int_Tecla3(void)
{
	ESTADOTECLA3=!ESTADOTECLA3;
	ESTADOTECLA2=OFF;
	ESTADOTECLA1=OFF;
}


int main(void)
{
/*DECLARACIONES*/
		gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};   /* Vector que configura el usuario y se lo pasa al driver display*/
		AD_conversor.input=CH1;
		AD_conversor.mode=AINPUTS_SINGLE_READ;
		AD_conversor.pAnalogInput=ptr_int_ad_conversor;

/*INICIALIZAICIONES*/
		LedsInit();
		ITSE0803Init(pines);
		SystemClockInit();
		UartInit(&puerto_PC); /*iNICIALIZO EL PUERTO DEL CONVERSOR*/

	   timmerA.timer=TIMER_A;  /*CONFIGURO EL TIMMER A con un periodo de 4 ms   PARA ELLO SE MODIFICO EL DRIVER TIMER PARA AUMRNTAR SU RESOLUCION*/
	   timmerA.period=4; /* EL TIMMER INTERRUMPE CADA 4 ms -> frecuencia muestreo=250 Hz */
	   timmerA.pFunc=ptr_int_timmerA;
	   TimerInit(&timmerA);
	   TimerStart(TIMER_A); /*Arrancamos el timmer A */

	   SwitchActivInt(SWITCH_1,Int_Tecla1);
	   SwitchActivInt(SWITCH_2,Int_Tecla2);
	   SwitchActivInt(SWITCH_3,Int_Tecla3);

	   AnalogInputInit(&AD_conversor);
	   AnalogOutputInit();

    while(1)
    {
    	if((ESTADOTECLA1==ON)&&(DATOS_CALCULADOS==ON))
    	{
			UartSendString(SERIAL_PORT_PC, UartItoa(PRESION_MAXIMA, DECIMAL));   /*Mando mediante uart a la pc la presion maxima*/
			UartSendBuffer(SERIAL_PORT_PC," mmHg\n\r", strlen(" mmHg \n\r"));

			ITSE0803DisplayValue(PRESION_MAXIMA);	/*Mando el valor de la presion maxima al LCD Display*/

			LedOn(LED_RGB_R); /* Prendo led rojo para decir que informo la presion maxima*/
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_G);

			DATOS_CALCULADOS=OFF;
    	}

    	if((ESTADOTECLA2==ON)&&(DATOS_CALCULADOS==ON))
    	{
   			UartSendString(SERIAL_PORT_PC, UartItoa(PRESION_MINIMA, DECIMAL));   /*Mando mediante uart a la pc la presion minima*/
   			UartSendBuffer(SERIAL_PORT_PC," mmHg\n\r", strlen(" mmHg \n\r"));

   			ITSE0803DisplayValue(PRESION_MAXIMA);	/*Mando el valor de la presion manima al LCD Display*/

   			LedOn(LED_RGB_R);   /* Prendo led rojo y verde (amarillo en RGB) para decir que informo la presion maxima*/
    		LedOn(LED_RGB_G);

    		DATOS_CALCULADOS=OFF;
    	 }

    	if((ESTADOTECLA2==ON)&&(DATOS_CALCULADOS==ON))
       	{
    		UartSendString(SERIAL_PORT_PC, UartItoa(PRESION_MINIMA, DECIMAL));   /*Mando mediante uart a la pc la presion promedio*/
   			UartSendBuffer(SERIAL_PORT_PC," mmHg\n\r", strlen(" mmHg \n\r"));

    		ITSE0803DisplayValue(PRESION_MAXIMA);	/*Mando el valor de la presion promedio al LCD Display*/

   			LedOff(LED_RGB_R);   /* Prendo led verde para decir que informo la presion promedio*/
   			LedOn(LED_RGB_G);

   			DATOS_CALCULADOS=OFF;
    	}

    	if((PRESION_MAXIMA>TOPE_MAXIMO)&&(DATOS_CALCULADOS==ON))
    	{
    		LedOn(LED_RGB_R);			 /*Si la pesrion esta por encima de 150 mmHg prendo led rojo*/
    	}
    	else
    	{
    		if((PRESION_MAXIMA>TOPE_MINIMO)&&(PRESION_MAXIMA<TOPE_MAXIMO)&&(DATOS_CALCULADOS==ON))
			{
    			LedOff(LED_RGB_R);     /*Si la pesrion esta por encima de 50 mmHg y por debajo de 150 mmHg prendo led azul*/
    			LedOff(LED_RGB_G);
    			LedOn(LED_RGB_B);
			}
    		else
    		{
    			LedOn(LED_RGB_G);  /*Caso contrario a los 2 anteriores. Si la pesrion esta por debajo de 50 mmHg prendo led verde*/
    			LedOff(LED_RGB_B);
    			LedOff(LED_RGB_R);
    		}

    	}

     }

}


/*==================[end of file]============================================*/
