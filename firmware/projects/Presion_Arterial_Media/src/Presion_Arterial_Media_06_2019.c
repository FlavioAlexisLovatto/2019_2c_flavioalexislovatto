/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * FAL	Flavio Alexis Lovatto
 *
 *
 *
 * Revisión:
 * 26-10-19: Versión inicial

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Presion_Arterial_Media_06_2019.h"

#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "switch.h"
#include "analog_io.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"  /*La aplicacion no deberia conocer a gpio, pero como ningun driver utilizado la incluye debemos incluirla
para que funcione el toggleo de un gpio*/

/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define BAUDRATE 57600
#define DECIMAL 10
#define FM 100 						/*Frecuencia de muestreo ->> en 1 s tenemos 100 muestras */
#define TIEMPO_DE_ADQUISICION 1 	/*  TIEMPO QUE SE ADQUIERE LA SEÑAL EN SEGUNDOS ->> PARA CALCULAR LA CANTIDAD DE MUESTRAS*/

/*==================[internal data definition]===============================*/

int b=0;
uint16_t VALOR=0;				/*Variable donde se guarda la conversion del AD*/
uint16_t  PRESIONES[100]; 		/* VECTOR QUE GUARDAD LAS PRESIONES CADA 10 ms*/
uint16_t AUX=0; 				/*VARIABLE A UTILIZAR PARA EN CONTRAR LA MAXIMA PRESION ARTERIAL */
uint16_t MAXIMO=0; 				/*VARIABLE A GUARDAR LA MAXIMA PRESION EN 1 SEGUNDO*/
uint16_t CANT_MUESTRAS=100; 	/*DEPENDE DE LA FREC MUESTREO Y EL TIEMPO DE ADQUISICION*/
uint8_t BANDERA_FLAG=0; 		/*BANDERA DE ESTADO DEL FLAG -> SI USO LA FUNCION GPIOActivInt*/
gpio_t pin;
analog_input_config AD_conversor;
timer_config timmerA; 			/* Para adquirir datos en 1 seg ->> trabaja a 10 ms para 100 muestras*/
serial_config puerto={SERIAL_PORT_PC,BAUDRATE,NO_INT};
uint16_t VALOR_MAX_BINARIO=1023; 		/* ES 2 A LA N, CON N NUM DE BITS, EN ESTE CASO 10 -> LA EDU CIAA*/

/*==================[internal functions declaration]=========================*/

/* funcion que opera con el vector PRESIONES (global) y calcula el maximo de todas las presiones EN 1 SEG*/
uint16_t maximo (void)
{
		int i;
		AUX=0;
		for(i=0;i<CANT_MUESTRAS;i++)
		{
			if(PRESIONES[i]>AUX)
				{
				AUX=PRESIONES[i];
				}
		}

		return(AUX);
}
/*==================[external data definition]===============================*/

/*Funcion que permite arrancar el timer para empezar a adquirir por flanco descendente de gpio07*/
  void ADQUIRIR(void)
  {
  if(BANDERA_FLAG==OFF)
  	{
  	TimerStart(TIMER_A);
  	}
  }


void ptr_int_func1(void)  /* modo adquisicion */
{
	LedOn(LED_RGB_G);
	LedOff(LED_RGB_R);
	BANDERA_FLAG=OFF;

	/* ESTA FUNCION LLAMA A ADQUIRIR SI SE CUMPLE QUE ESTE EN 0 EL T_COL0 CON LA INTERRUP DEL GPIOGP7 */
	GPIOActivInt(GPIOGP7,T_COL0,ADQUIRIR, 0);
}

void ptr_int_func2(void)  /*modo apagado*/
{
	LedOn(LED_RGB_R);
	LedOff(LED_RGB_G);
	TimerStop(TIMER_A);
	ITSE0803DisplayValue(OFF);
	BANDERA_FLAG=ON;
}


void ptr_int_display_timmerA(void)
{
	AnalogStartConvertion();   /*ARRANCO A CONVERTIR CADA 10 ms */
}

void ptr_int_ad_conversor(void)
{
	AnalogInputRead(CH1, &VALOR);
	PRESIONES[b]=VALOR;
	b++;
	if(b==CANT_MUESTRAS)
	{
		b=0;
		MAXIMO=maximo();
		MAXIMO=(50+(75*(MAXIMO/VALOR_MAX_BINARIO)));
		UartSendString(SERIAL_PORT_PC, UartItoa(MAXIMO, DECIMAL));
		UartSendBuffer(SERIAL_PORT_PC," mmHg \n\r", strlen(" mmHg \n\r")); /* FORMATO EN EL QUE SE ENVIA LA MAXIMA PRESION ARTERIAL*/
		ITSE0803DisplayValue(MAXIMO);
		TimerStop(TIMER_A);
	}

}



/*==================[external functions declaration]==========================*/


int main(void)
{
	/*DECLARACIONES*/
	AD_conversor.input=CH1; /*SE CONFIGURA EL CH1 PARA RECIBIR LA SEÑAL A CONVERTIR*/
	AD_conversor.mode=AINPUTS_SINGLE_READ;
	AD_conversor.pAnalogInput=ptr_int_ad_conversor;
	gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; /* Vector que configura el usuario y se lo pasa al driver display*/

	/*INICIALIZAICIONES*/


   ITSE0803Init(pines);
   SystemClockInit();
   LedsInit();

   SwitchesInit();
   SwitchActivInt(SWITCH_1, ptr_int_func1);
   SwitchActivInt(SWITCH_2, ptr_int_func2);

   timmerA.timer=TIMER_A;
   timmerA.period=10;     /*configuramos el timmer A con un periodo de 10 ms PARA QUE ADQUIERA LA SEÑAL */
   timmerA.pFunc=ptr_int_display_timmerA;
   TimerInit(&timmerA);
   /*El timerStart no esta aqui ya que se arranca si se recibe un flanco descendente del GPIO07*/

   AnalogInputInit(&AD_conversor);
   AnalogOutputInit();

   LedOn(LED_RGB_R); /* arranca en modo apagado -> BANDERA DE FLAG EN ALTO*/
   LedOff(LED_RGB_G);
   BANDERA_FLAG=ON;


    while(1)
    {


     }

}


/*==================[end of file]============================================*/
