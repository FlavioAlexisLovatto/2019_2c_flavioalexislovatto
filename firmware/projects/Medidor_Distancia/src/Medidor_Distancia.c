/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * Lovatto Flavio Alexis   flovatto@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "Medidor_Distancia.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "hc_sr4.h"
#include "DisplayITS_E0803.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
#define ON 1
#define OFF 0
/*==================[internal data definition]===============================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	/*DECLARACIONES*/
	uint8_t teclas;
	uint8_t  sensor_distancia=0;
	gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; /* Vector que configura el usuario y se lo pasa al driver display*/
	uint16_t ESTADOTECLA1=0; /* ESTADOTECLA1= ESTADO DE LA TECLA 1*/
	uint16_t MEDICIONCENTIMETROS=1; /* BANDERAS PARA SABER SI MIDO EN CM O IN */
	uint16_t MEDICIONPULGADAS=0;
	uint16_t HOLD=0; /* VARIABLE PARA SABER SI MIDO CONTINUAMENTE O PAUSO LA MEDIDA */

	/* INICIALIZAICIONES */
	ITSE0803Init(pines);
	HcSr04Init(T_FIL2, T_FIL3); /* ​T_FIL2 para inicializar el echo y T_FIL3 para el trigger */
	SystemClockInit();
	LedsInit();
	SwitchesInit();


	while(1)
    {
		while(ESTADOTECLA1==1)
		{
			sensor_distancia=HcSr04ReadDistanceCentimeters();
			ITSE0803DisplayValue(sensor_distancia);

			if(MEDICIONCENTIMETROS==1)
			{
				sensor_distancia=HcSr04ReadDistanceCentimeters();
    		 	ITSE0803DisplayValue(sensor_distancia);}
			if(MEDICIONPULGADAS==1)
			{
				sensor_distancia=HcSr04ReadDistanceInches();
    		    ITSE0803DisplayValue(sensor_distancia);
			}

			if(SwitchesRead()==1)
			{
				DelayMs(100);
				ITSE0803DisplayValue(OFF);
				ESTADOTECLA1=0;
    	    }

			if(SwitchesRead()==2)
			{
				DelayMs(100);
   	 		    HOLD=1;
   	 		    while(HOLD==1)
   	 		    {
   	 		    	ITSE0803DisplayValue(sensor_distancia);
   	 		    	if(SwitchesRead()==2)
   	 		    	{
   	 		    		DelayMs(100);
    	 	 	        ESTADOTECLA1=0;
    	 	 	        HOLD=0;
   	 		    	}

   	 		    }
    	 	}


    	}
/* PARTE DEL DISPLAY DE LA TECLA 2;*/
		while(HOLD==1)
		{
			ITSE0803DisplayValue(sensor_distancia);
    		if(SwitchesRead()==(1<<1))
    		{
    			DelayMs(100);
    			HOLD=0;

    	   	}
		}

    	teclas  = SwitchesRead();
    	DelayMs(100);
    	switch(teclas)
    	{
    		case SWITCH_1:
    			ESTADOTECLA1=1;
    		break;

    		case SWITCH_2:
    			HOLD=1;
    		break;

    		case SWITCH_3:
    			MEDICIONCENTIMETROS=1; /* BANDERA PARA SABER SI MIDO EN CM O IN */
				MEDICIONPULGADAS=0;
    		break;

    		case SWITCH_4:
    			MEDICIONPULGADAS=1;
   				MEDICIONCENTIMETROS=0;
    		break;
    	}

	}

}



/*==================[end of file]============================================*/

