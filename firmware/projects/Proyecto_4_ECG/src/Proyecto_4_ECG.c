/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * FALovatto	flovatto@ingenieria.uner.edu.ar
 *
 * Revisión:
 * 25-10-19: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_4_ECG.h"
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "DisplayITS_E0803.h"
#include "analog_io.h"
#include "Delay.h"
#include "led.h"

/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
#define PeriodoQRS 120
#define DECIMAL 10
#define BAUDRATE 115200
analog_input_config AD_conversor;
timer_config timmer;
uint16_t VALOR=0;
uint16_t MAX=600; 					/*eL RANGO DINAMICO DE LA PLACA ES DE 10 BITS, POR LO QUE E LMAXIMO VALRO SERA 1024, CONSIDERANDO UN VALOR
									DE PICO QRS DE 700 EMPEZAMOS A COMPARAR */
uint16_t FC=0;  					/* variable de frecuencia cardica, cuenta los picos qrs para obtener en 1 min*/
uint16_t CONTADOR=0;
uint16_t CTE_A_MIN=60*1000;	 		/*CTE PARA PASAR LOS ms A MIN EN LA OBTENCION DE LA FC*/
uint16_t CONT_FC=0;    				/*VARIABLE A CONTROLAR QUE NO NOS PASEMOS DEL MINUTO PARA OBTENER LA FC*/
uint16_t valor_ant_ant=0; 			/* variable auxiliar para el detector de flancos*/
uint16_t valor_ant=0; 				/* variable para detectar flanco*/
uint8_t bandera_pico=0;
serial_config puerto={SERIAL_PORT_PC,BAUDRATE,NO_INT};

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

void ptr_int_timmer(void)
{
	AnalogStartConvertion();
	CONT_FC=CONT_FC+4;			/*AUMENTO EL CONTADOR EN 4 ms QUE ES CADA CUANTO TOMAMOS UNA MUESTRA*/
	LedOff(LED_1);
 /*LA INTERRUPCION DEL TIMMER ME DA LA FREC A LA CUAL CONVIERTO Y LUEGO INCREMENTO UN CONTADOR GLOBAL -> PARA OBTENER LA FC */
}

void ptr_int_ad_conversor(void)
{
	AnalogInputRead(CH1, &VALOR);
	if(VALOR>MAX)
	{
		if((valor_ant_ant<valor_ant)&&(VALOR<valor_ant))
	    {
			 LedOn(LED_1);
			 bandera_pico=ON;
		}
		valor_ant_ant=valor_ant;
		valor_ant=VALOR;
	 }
}
/*LA INTERRUPCION DEL CONVERSOR ME GUARDA LA CONVERSION EN LA VARIABLE GLOBAL "VALOR"*/

/*==================[external functions declaration]==========================*/


int main(void)
{
	/*DECLARACIONES*/
	gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; /* Vector que configura el usuario y se lo pasa al driver display*/
	AD_conversor.input=CH1;
	AD_conversor.mode=AINPUTS_SINGLE_READ;
	AD_conversor.pAnalogInput=ptr_int_ad_conversor;

	/*INICIALIZAICIONES*/
	LedsInit();
	LedOn(LED_RGB_R); /*Inicia con led rojo prendido*/
	ITSE0803Init(pines);
	SystemClockInit();
	UartInit(&puerto);

   timmer.timer=TIMER_A;
   timmer.period=4; /* EL TIMMER INTERRUMPE CADA 4 ms -> frecuencia muestreo=250 Hz */
   timmer.pFunc=ptr_int_timmer;
   TimerInit(&timmer);
   TimerStart(TIMER_A); /*arrancamos el timmer a */

   AnalogInputInit(&AD_conversor);
   AnalogOutputInit();

    while(1)
    {
    	if(bandera_pico==ON)
    	{
    		FC=CTE_A_MIN; 			/* obtenemos la fc como la inversa del periodo del QRS, multiplicando previamente la misma por una cte*/
    		FC=(FC/CONT_FC);
    		CONT_FC=0;
    		bandera_pico=OFF;
		     	 	 	 	 	 /*AHORA PRENDEMOS UN LED AZUL SI LA PERSONA TIENE BRADICARDIA (FC<60 LAT/MIN) O UN LED ROJO CON TAQUICARDIA (FC> 120)*/
    		if(FC<60)
    		{
    			LedOn(LED_RGB_B);
    		}
			else
			{
				LedOff(LED_RGB_B);
			}

    		if(FC>120)
    		{
				LedOn(LED_RGB_R);
    		}
			else
			{
				LedOff(LED_RGB_R);
			}

			/*TRASMITIMOS A LA PC POR UART */
    		UartSendString(SERIAL_PORT_PC, UartItoa(FC, DECIMAL));
	    	UartSendBuffer(SERIAL_PORT_PC, " LAT/MIN \n\r", strlen(" LAT/MIN \n\r"));
    	}
    }
}




/*==================[end of file]============================================*/
