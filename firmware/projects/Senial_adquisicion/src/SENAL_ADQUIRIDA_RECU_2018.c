/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 * LF 	Lovatto, Flavio
 *
 *
 *
 *
 * Revisión:
 * 25-09-19: Versión inicial

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "analog_io.h"
#include "../inc/SENAL_ADQUIRIDA_RECU_2018.h"
#include "gpio.h"  /*La aplicacion no deberia conocer a gpio, pero como ningun driver utilizado la incluye debemos incluirla
					para que funcione el toggleo de un gpio*/

/*==================[macros and definitions]=================================*/

#define ON 1					/*Banderas de Alto y Bajo*/
#define OFF 0

#define BAUDRATE 57600
#define DECIMAL 10

#define VALOR_MAX_BINARIO 1023       /*Utilizado a la hora de convertir la presion es 2^n y el conversor es de n=10 bits*/

/*gpio_t pin; NO LA USAMOS PASAMOS DIRECTO EN EL GPIO DE ABAJO EN INICIALIZACIONES*/


/*==================[internal data definition]===============================*/

uint16_t  BANDERA_TOGGLE=0;			/*Bandera para togglear*/
uint16_t  VALOR=0;
uint16_t  PROMEDIO=0;
uint16_t  ACUMULADOR[100];
uint8_t CANTIDAD_MUESTRAS=100; 		/* (FM*TIEMPO_DE_ADQUISICION) Sujeto al tiempo de adquisicion y a la frecuencia de muestreo*/
uint8_t AUX=0;
/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*Función que me devuelve el promedio del vector con los datos*/
uint16_t Promedio(void)
{
	int i;
	AUX=0;
	for(i=0;i<CANTIDAD_MUESTRAS;i++)
	{
		AUX=AUX+ACUMULADOR[i];
	}
	AUX=AUX/CANTIDAD_MUESTRAS;
	return(AUX);
}


void ptr_int_display_timmerA(void)
{
	AnalogStartConvertion();
}


void ptr_int_ad_conversor(void)
{
	AnalogInputRead(CH1, &VALOR);

	/*Guardo los datos en el vector*/
	ACUMULADOR[b]=(VALOR/VALOR_MAX_BINARIO);
	b++;
	if(b==CANTIDAD_MUESTRAS)
	{
		b=0;
		PROMEDIO=Promedio();
		UartSendString(SERIAL_PORT_PC, UartItoa(PROMEDIO, DECIMAL));
		UartSendBuffer(SERIAL_PORT_PC," ES EL PROMEDIO \n\r", strlen(" ES EL PROMEDIO \n\r")); /* FORMATO EN EL QUE SE ENVIA EL PROMEDIO*/
		BANDERA_TOGGLE=!(BANDERA_TOGGLE);
	}
}

/*==================[external functions declaration]==========================*/


int main(void)
{
	/*DECLARACIONES*/
	AD_conversor.input=CH1; 				/*SE CONFIGURA EL CH1 PARA RECIBIR LA SEÑAL A CONVERTIR*/
	AD_conversor.mode=AINPUTS_SINGLE_READ;
	AD_conversor.pAnalogInput=ptr_int_ad_conversor;

	/*INICIALIZAICIONES*/
	analog_input_config AD_conversor;
	timer_config timmerA; 				/* Para adquirir datos en 1 seg ->> trabaja a 10 ms para 100 muestras*/
	serial_config puerto={SERIAL_PORT_PC,BAUDRATE,NO_INT};

	GPIOInit(T_COL0, GPIO_OUTPUT); 			/*CONFIGURO EL GPIO T_COL0 A TOGGLEAR COMO SALIDA*/
   SystemClockInit();
   LedsInit();

   timmerA.timer=TIMER_A;
   timmerA.period=10;     					/*configuramos el timmer A con un periodo de 10 ms PARA QUE ADQUIERA LA SEÑAL */
   timmerA.pFunc=ptr_int_display_timmerA;
   TimerInit(&timmerA);
   TimerStart(TIMER_A); 					/*arrancamos el timmer A */

   AnalogInputInit(&AD_conversor);
   AnalogOutputInit();


    while(1)
    {
    	if(BANDERA_TOGGLE==OFF)
    	{
    		LedOff(LED_RGB_B);			/* CUANDO EL LED ESTE APAGADO LA SALIDA EN T_COL0 SERA 0*/
    		GPIOOff(T_COL0);
    	}
    	if(BANDERA_TOGGLE==ON)
    	{
      		LedOn(LED_RGB_B);			/*CUANDO EL LED ESTE PRENDIDO LA SALIDA EN T_COL0 SERA 1*/
      		GPIOOn(T_COL0);
    	}

     }
}





/*==================[end of file]============================================*/
