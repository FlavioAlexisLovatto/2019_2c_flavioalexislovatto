/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es: Sánchez, Franco
 * 			 Lovatto, Flavio
 *
 *
 *
 * Revisión:
 * 30-08-19: Versión inicial
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../sensor_conteo_objetos/inc/sensor_conteo_obejtos.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "tcrt5000.h"
#include "bool.h"


/*==================[macros and definitions]=================================*/
#define Numero_max_productos  500; //En 500 segundos, son 1 seg por objeto.//

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	gpio_t pin=T_COL0; /* Se recomienda usar el T_COL0 para que trabaje con el sensor, se encuentra en el pin fisico n° 48*/
	Tcrt5000Init(pin);
	bool estado;
	int cont=0; /* cont representa el numero de conteos de los objetos*/

	SystemClockInit();
	LedsInit();
	SwitchesInit();

    while(1)
    {
    	estado=Tcrt5000State();
    	if(estado==true)
    	{
    		DelayMs(100);
    		while(estado==true)
    		{
    			LedOn(LED_1);					/* cuando no se cuenta un objeto se prende el led 1*/
    			DelayMs(100);
    			estado=Tcrt5000State();
    		}
    	}
    	else
    	{
    		cont++;
    		estado=Tcrt5000State();
    		DelayMs(100);
    		while(estado==false)
    		{
    			DelayMs(100);
    			estado=Tcrt5000State();
    			LedOff(LED_1);
    		}
    		/* Cada vez que se lee se espera con un delay 100 ms para evitar lo transitorios de la señal*/
    		/*cuando se cuento un objeto se  mantiene apagado el led1 hasta que se deje observar el mismo*/
    	}
    }
/*==================[end of file]============================================*/

