/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * SF 		Sánchez, Franco
 * LF 		Lovatto, Flavio
 *
 *
 *
 * Revisión:
 * 5-10-19: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* 	ENUNCIADO
 *  Se desea implementar, utilizando el driver de LCD, un cronómetro que pueda contar hasta 9 minutos.
 *  Se utiliza la tecla 2 para comenzar la cuenta, la tecla 4 para pausarla,
 *  la tecla 3 para reiniciarla y la tecla 1 para mostrar las centésimas.
 *  A partir de la aplicación provista por la cátedra (“cronómetro”) que realiza las acciones descriptas,
 *  realice las modificaciones necesarias para que el cronómetro “cuente” de forma descendente.
 * */


/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "string.h"
#include "Cronometro_LCD.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/

#define  ON 1
#define  OFF 0
const uint16_t nueve_minutos=859;
const uint16_t un_minuto=60;

/*banderas globales */
	uint16_t ESTADOTECLA1=0; /* ESTADOTECLA1=1, APRETE LA TECLA 1 Y MUESTRO LAS CENTESIMAS*/
	uint16_t ESTADOTECLA2=0;
	uint16_t ESTADOTECLA3=0;

	uint16_t HOLD=0; /* VARIABLE DE LA TECLA 4 */
	uint16_t  SEGUNDOS=0;
	uint16_t  MINUTOS=0;
	uint16_t VALOR=0;
	timer_config timmer;


/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/
/*==================[external data definition]===============================*/

   void ptr_int_func1(void){
	   ESTADOTECLA1=!ESTADOTECLA1;

						}
	void ptr_int_func2(void){
		ESTADOTECLA2=!ESTADOTECLA2;

	}

	void ptr_int_func3(void){
	SEGUNDOS=0;
	MINUTOS=0;
							}

	void ptr_int_func4(void){
		HOLD=!HOLD;
	}

void ptr_int_display_timmer(void){

	if((ESTADOTECLA2==ON)&(HOLD==OFF)){

		if(SEGUNDOS<un_minuto){
		VALOR=SEGUNDOS+MINUTOS;
		SEGUNDOS++;
		}

		else{

		SEGUNDOS=0;
		MINUTOS=MINUTOS+100;
		VALOR=SEGUNDOS+MINUTOS;
		}

		if(MINUTOS>nueve_minutos)
		{	SEGUNDOS=0;
			MINUTOS=0;
		}
		ITSE0803DisplayValue(VALOR);

	}
	        if(HOLD==ON){
	        		     ITSE0803DisplayValue(VALOR);
	        }







}

/*==================[external functions declaration]==========================*/


int main(void)
{
	/*DECLARACIONES*/

	gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; /* Vector que configura el usuario y se lo pasa al driver display*/

	 /*INICIALIZAICIONES*/

	ITSE0803Init(pines);

	SystemClockInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, ptr_int_func1);
	SwitchActivInt(SWITCH_2, ptr_int_func2);
	SwitchActivInt(SWITCH_3, ptr_int_func3);
	SwitchActivInt(SWITCH_4, ptr_int_func4);


   timmer.timer=TIMER_B;  /*configuramos el timmer B con un periodo de 1000 */
   timmer.period=1000;
   timmer.pFunc=ptr_int_display_timmer;
   TimerInit(&timmer);
   TimerStart(TIMER_B); /*arrancamos el timmer b */




    while(1)
    {

    }

}





/*==================[end of file]============================================*/
