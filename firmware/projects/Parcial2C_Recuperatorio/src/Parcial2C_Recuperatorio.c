/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es:
 *	LF		Lovatto, Flavio
 *
 *
 *
 * Revisión:
 * 19-11-19: Versión inicial

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial2C_Recuperatorio.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "switch.h"
#include "analog_io.h"
#include "DisplayITS_E0803.h"

/*==================[macros and definitions]=================================*/

#define ON 1				/*Banderas de alto o bajo*/
#define OFF 0

#define BAUDRATE 115200     /*Como no se especifica la velocidad de transmición tomo por defecto esta, que concidero rapida y suficiente en esta app*/
#define DECIMAL 10

#define LPF_BETA1 25	/*Valores del parametro que caracteriza el filtrado, solo parte decimal*/
#define LPF_BETA2 55
#define LPF_BETA3 75

/*==================[internal data definition]===============================*/
analog_input_config AD_conversor;
timer_config timmerA;
serial_config puerto_PC={SERIAL_PORT_PC,BAUDRATE,NO_INT};

uint16_t VALOR_FILTRADO=0; 			/*Variable para guardar el valor filtrado*/
uint16_t VALOR_FILTRADO_AUX=0;		/*Variable para el valor filtrado en [n-1]*/
uint16_t VALOR_CRUDO=0; 			/*Variable donde se guarda la conversion del AD*/

uint8_t ESTADOTECLA2=0;				/*La tecla 2 utilizo para seleccionar lpf_beta de 0.25*/
uint8_t ESTADOTECLA3=0;				/*La tecla 3 utilizo para seleccionar lpf_beta de 0.55*/
uint8_t ESTADOTECLA4=0;				/*La tecla 4 utilizo para seleccionar lpf_beta de 0.75*/
float LPF_BETA=0.0; 				/*Valor del parametro que caracteriza el filtrado, dependiendo de la tecla que aprete va a variar su valor*/
/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

/*==================[external functions declaration]==========================*/


/*Interrupcion del timer*/
void ptr_int_timmerA(void)
{
	AnalogStartConvertion();   /*ARRANCO A CONVERTIR CADA 10 ms */
}

/*Interrupcion del conversor y en la misma voy haciendo el filtrado dependiendo el parametro elegido y mando por puerto serie los datos*/
void ptr_int_ad_conversor(void)
{
		AnalogInputRead(CH1, &VALOR_CRUDO);

		/*El valor del lpf_beta va a ser distinto en cada if dependiendo de que tecla se aprete */
		if((ESTADOTECLA2==ON)&&(ESTADOTECLA3==OFF)&&(ESTADOTECLA4==OFF))
		{
			VALOR_FILTRADO=(VALOR_FILTRADO_AUX)-(LPF_BETA*(VALOR_FILTRADO_AUX-VALOR_CRUDO));
			VALOR_FILTRADO_AUX=VALOR_FILTRADO;

			UartSendString(SERIAL_PORT_PC, UartItoa(VALOR_CRUDO, DECIMAL));			/*Mando mediante uart a la pc el valor crudo*/
			UartSendBuffer(SERIAL_PORT_PC," , ", strlen(" , "));					/*Pongo la coma*/
			UartSendString(SERIAL_PORT_PC, UartItoa(VALOR_FILTRADO, DECIMAL));  	/*Mando luego de la coma el valor filtrado*/
			UartSendBuffer(SERIAL_PORT_PC,"\n\r", strlen("\n\r"));					/*Mando el salto de linea*/
		}
		if((ESTADOTECLA2==OFF)&&(ESTADOTECLA3==ON)&&(ESTADOTECLA4==OFF))
		{
			VALOR_FILTRADO=(VALOR_FILTRADO_AUX)-(LPF_BETA*(VALOR_FILTRADO_AUX-VALOR_CRUDO));
			VALOR_FILTRADO_AUX=VALOR_FILTRADO;

			UartSendString(SERIAL_PORT_PC, UartItoa(VALOR_CRUDO, DECIMAL));			/*Mando mediante uart a la pc el valor crudo*/
			UartSendBuffer(SERIAL_PORT_PC," , ", strlen(" , "));					/*Pongo la coma*/
			UartSendString(SERIAL_PORT_PC, UartItoa(VALOR_FILTRADO, DECIMAL));  	/*Mando luego de la coma el valor filtrado*/
			UartSendBuffer(SERIAL_PORT_PC,"\n\r", strlen("\n\r"));					/*Mando el salto de linea*/
		}

		if((ESTADOTECLA2==OFF)&&(ESTADOTECLA3==OFF)&&(ESTADOTECLA4==ON))
		{
			VALOR_FILTRADO=(VALOR_FILTRADO_AUX)-(LPF_BETA*(VALOR_FILTRADO_AUX-VALOR_CRUDO));
			VALOR_FILTRADO_AUX=VALOR_FILTRADO;

			UartSendString(SERIAL_PORT_PC, UartItoa(VALOR_CRUDO, DECIMAL));			/*Mando mediante uart a la pc el valor crudo*/
			UartSendBuffer(SERIAL_PORT_PC," , ", strlen(" , "));					/*Pongo la coma*/
			UartSendString(SERIAL_PORT_PC, UartItoa(VALOR_FILTRADO, DECIMAL));  	/*Mando luego de la coma el valor filtrado*/
			UartSendBuffer(SERIAL_PORT_PC,"\n\r", strlen("\n\r"));					/*Mando el salto de linea*/
		}
}

/*Interrupcion para la tecla 1*/
void Int_Tecla2(void)
{
	ESTADOTECLA2=!ESTADOTECLA2;
	ESTADOTECLA3=OFF;
	ESTADOTECLA4=OFF;
	LPF_BETA=0.25;
}

/*Interrupcion para la tecla 2*/
void Int_Tecla3(void)
 {
	ESTADOTECLA3=!ESTADOTECLA3;
	ESTADOTECLA2=OFF;
	ESTADOTECLA4=OFF;
	LPF_BETA=0.55;
 }

/*Interrupcion para la tecla 3*/
void Int_Tecla4(void)
{
	ESTADOTECLA4=!ESTADOTECLA4;
	ESTADOTECLA2=OFF;
	ESTADOTECLA3=OFF;
	LPF_BETA=0.75;
}


int main(void)
{
/*DECLARACIONES*/
	gpio_t pines[7]= {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};   /* Vector que configura el usuario y se lo pasa al driver display*/
	AD_conversor.input=CH1;
	AD_conversor.mode=AINPUTS_SINGLE_READ;
	AD_conversor.pAnalogInput=ptr_int_ad_conversor;

/*INICIALIZAICIONES*/
	SystemClockInit();
	ITSE0803Init(pines);
	UartInit(&puerto_PC); /*INICIALIZO EL PUERTO DEL CONVERSOR*/

	timmerA.timer=TIMER_A;	/*CONFIGURO EL TIMMER A con un periodo de 10 ms, modifique el driver del timer asi pongo directo los ms y tambien puedo tener más resolución si necesito*/
	timmerA.period=10; 		/* EL TIMMER INTERRUMPE CADA 10 ms -> frecuencia muestreo=100 Hz */
	timmerA.pFunc=ptr_int_timmerA;
	TimerInit(&timmerA);
	TimerStart(TIMER_A); /*Arrancamos el timmer A */

	SwitchActivInt(SWITCH_2,Int_Tecla2);
	SwitchActivInt(SWITCH_3,Int_Tecla3);
	SwitchActivInt(SWITCH_4,Int_Tecla4);

	AnalogInputInit(&AD_conversor);
	AnalogOutputInit();

    while(1)
    {

    	if((ESTADOTECLA2==ON)&&(ESTADOTECLA3==OFF)&&(ESTADOTECLA4==OFF))
    	{
			ITSE0803DisplayValue(LPF_BETA1);	/*Mando el valor del parametro 0.25 al LCD Display*/
    	}

		if((ESTADOTECLA2==OFF)&&(ESTADOTECLA3==ON)&&(ESTADOTECLA4==OFF))
    	{
   			ITSE0803DisplayValue(LPF_BETA2);	/*Mando el valor del parametro 0.55 al LCD Display*/
    	}

		if((ESTADOTECLA2==OFF)&&(ESTADOTECLA3==OFF)&&(ESTADOTECLA4==ON))
       	{
    		ITSE0803DisplayValue(LPF_BETA3);	/*Mando el valor parametro 0.75 al LCD Display*/
    	}
    }

}


/*==================[end of file]============================================*/
