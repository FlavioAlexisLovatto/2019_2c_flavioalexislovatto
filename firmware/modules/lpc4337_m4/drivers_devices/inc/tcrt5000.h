/* Copyright 2019,

 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef TCRT5000_H
#define TCRT500_H
/** @brief Bare Metal header for switches in EDU-CIAA NXP
 **
 ** This is a driver for four switches mounted on the board
 **
 **/

/** @addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** @addtogroup Sources_LDM Leandro D. Medus Sources
 ** @{ */
/** @addtogroup Baremetal Bare Metal source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * SF			Sánchez, Franco
 * FL			Lovatto, Flavio
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 30/08/19 1 initials initial version

 */

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <gpio.h>
/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/
bool Tcrt5000Init(gpio_t dout);
/*La función sirve para configurar un determinado pin para trabajar con el sensor.*/

bool Tcrt5000State(void);
/*Esta funcion devuelve el estado en que se encuentrar el pin que se configuro en la CIAA para recibir información del sensor*/

bool Tcrt5000Deinit(gpio_t dout);
/* La funcion recibe como parametro el pin del chip que va a desinicializar, para esto usara la funcin del gpio */

/** @brief Initialization function to control basic push-buttons in the EDU-CIAA BOARD
 * This function initialize the four switches present in the EDU-CIAA board
 * @return true if no error
 */


/** @brief Function to read basic push-buttons
 * The function returns a 8 bits word, where the first four binaries positions represent each push-button.
 * @return 0 if no key pressed, SWITCH_1 SWITCH_2 SWITCH_3 SWITCH_4 in other case.
 */


/** @brief Enables the interruption of a particular key and assigns a group of interrupts defined in @ref switchgp_t
 * @param[in] gp Indicates to which group of interrupts the key is assigned
 * @param[in] tec Key that will interrupt
 * @param[in] ptrIntFunc Function to be called in the interruption
 */


/** @brief Activate group interrupts and add to the interruption the chosen keys through the mask
 * @param[in] tecs Mask of keys that will be added to the group to interrupt. Where bit0-TEC4, bit1-TEC3, bit2-TEC2 and bit4-TEC1.
 * @param[in] void * Function to be called in the interruption.
 */


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef SWITCH_H */

