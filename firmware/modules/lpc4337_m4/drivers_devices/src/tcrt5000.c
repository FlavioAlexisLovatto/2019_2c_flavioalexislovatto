/* Copyright 2019,

 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Bare Metal driver for switchs in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  SF			Sánchez,Franco
	FL			Flavio, Lovatto
*/
/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 30/08/19 initials initial version

 */

/*==================[inclusions]=============================================*/
#include "switch.h"
#include "tcrt5000.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/
gpio_t pin_sensor;

/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/* TCRT5000Init CONFIGURA EL PIN QUE LE MANDES COMO ENTRADA, PARA QUE EL SENSOR PUEDA ENVIAR INFORMACION.*/
bool Tcrt5000Init(gpio_t dout)
{
	pin_sensor=dout;
	GPIOInit(dout, GPIO_INPUT);

	return true; 	/* DEVUELVE TRUE PARA SABER QUE EL CODIGO LLEGO HASTA ACA*/
}

/* Esta funcion me devuelve el estado en que se encuentra el sensor, si es alto no cuenta y si es bajo conto algo*/
bool Tcrt5000State(void){
	return GPIORead(pin_sensor); /* RETORNO EL VALOR LEIDO POR GPIO PARA CONOCER EL ESTADO */
}

/* CUMPLE LA FUNCION DE DESCINIZIALIZAR TODO , EN ESTE CASO USAMOS EL DEL GPIO*/
bool Tcrt5000Deinit(gpio_t dout)
{
	GPIODeinit();
	return true; 	/* retorno true el codigo llego hasta aca */
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
