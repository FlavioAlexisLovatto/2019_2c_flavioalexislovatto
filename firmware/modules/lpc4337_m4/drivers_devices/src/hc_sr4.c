/* Copyright 2019,

 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Bare Metal driver for switchs in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  SF			Sánchez,Franco
	FL			Flavio, Lovatto
*/
/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 30/08/19 initials initial version

 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/
gpio_t pin_echo;
gpio_t pin_trigger;
const  uint16_t resolucion=10; 	/* para que el sensor trabaje con una resolucion de 1 ms*/
uint16_t c=0;  					/* c representa el numero de conteos a multiplicar por la resolucion de delay elegido*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/* HcSr04Init CONFIGURA EL PIN DE ECHO COMO ENTRADA(RECIBO EL TIEMPO) Y EL TRIGGER COMO SALIDA(LE ENVIO EL PULSO).*/
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	pin_echo=echo;
	pin_trigger=trigger;
	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);
	return true; 					/* DEVUELVE TRUE PARA SABER QUE EL CODIGO LLEGO HASTA ACA*/
}

/*Esta funcion me devuelve 1/58 para en la aplicacion poder calcular el tiempo del echo en centimetros*/
int16_t HcSr04ReadDistanceCentimeters()
{
	int c=0;  	/* c representa el numero de conteos a multiplicar por la resolucion de delay elegido*/

	/* genero el pulso de 10 us que recibe el sensor de ultrasonido*/
	GPIOOn(pin_trigger);
	DelayUs(10);
	GPIOOff(pin_trigger);
	while(GPIORead(pin_echo)==false)
	{

	}

	while(GPIORead(pin_echo)==true)
	{
		DelayUs(resolucion); 	/* La resolucion sera de 1 us*/
		c++;
	}


	c=(c*resolucion); 	/* calculo el tiempo total del ciclo en alto del sensor de ultrasonido */

	return (c/58);
}

/*Esta funcion me devuelve el 1/148 para en la aplicacion calcular el tiempo del echo en pulgadas */

int16_t HcSr04ReadDistanceInches()
{
	int c=0;  /* c representa el numero de conteos a multiplicar por la resolucion de delay elegido*/

	/* genero el pulso de 10 us que recibe el sensor de ultrasonido*/
		GPIOOn(pin_trigger);
		DelayUs(10);
		GPIOOff(pin_trigger);
		while(GPIORead(pin_echo)==false)
		{

		}

		while(GPIORead(pin_echo)==true)
		{
			DelayUs(resolucion);  /* La resolucion sera de 1 us*/
			c++;
		}

		c=(c*resolucion);  /* calculo el tiempo total del ciclo en alto del sensor de ultrasonido */

		return (c/148);
}

/*Función para desinicializar*/
bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{
	GPIODeinit();
	return true;   /* retorno true el codigo llego hasta aca */
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
