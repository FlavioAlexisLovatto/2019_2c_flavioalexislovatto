	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LFA 			Lovatto Flavio Alexis
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Leando Medus
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include "DisplayITS_E0803.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/

/*cantidad de GPIO necesarios para configurar el LCD*/
#define CANTIDAD_GPIO 7

/*cantidad de digitos que muestra el LCP*/
#define CANTIDAD_DIGITOS 3

/*cantidad de byte*/
#define CANTIDAD_BYTE 4

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/* funcion que recibe un dato de 16 bit, lo separa en cada uno de sus bits y los convierte a Bcd,
devolviendolos a traves de la direccion de memoria del puntero *bcd_number del ultimo parámetro  */
void BinaryToBcd(uint16_t data, uint8_t digits, uint8_t *bcd_number);

/* función que toma un dígito BCD y lo separa en cada uno de sus bit y los guarda en los datos de LCD1 a LCD4 */
void DigitoABit(uint8_t bcd);


/*==================[internal data definition]===============================*/
/*vector de pines que pasa el usuario.
 *pines[0]=LCD1
 *pines[1]=LCD2
 *pines[2]=LCD3
 *pines[3]=LCD4
 *pines[4]=SEL1
 *pines[5]=SEL2
 *pines[6]=SEL3
 * */
gpio_t pines[7];

/*variable global que guarda el dato*/
uint16_t valor_numerico=0;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
void BinaryToBcd(uint16_t data, uint8_t digits, uint8_t *bcd_number)
{
	uint8_t i=0;
	while(i<digits)
	{
		bcd_number[i]=data%10;
		data=data/10;
		i++;
	}
}

void DigitoABit(uint8_t bcd)
{
	uint8_t i,aux;
	for(i=0;i<CANTIDAD_BYTE;i++)
	{
		aux=bcd&1;
		if(aux==0)
		{
			GPIOOff(pines[i]);
		}
		else
		{
			GPIOOn(pines[i]);
		}
			bcd=(bcd>>1);
	}
}

/*==================[external functions definition]==========================*/

/*función de inicializacion*/
bool ITSE0803Init(gpio_t * pins)
{
	uint8_t i;
	for(i=0; i<CANTIDAD_GPIO;i++)
	{
		pines[i]=pins[i];
		GPIOInit(pins[i],GPIO_OUTPUT);
	}
	return true;
}

/*función para visualizar valor*/
bool ITSE0803DisplayValue(uint16_t valor)
{
	valor_numerico=valor;
	uint8_t datos[3];
	BinaryToBcd(valor,CANTIDAD_DIGITOS,datos);
	uint8_t i;
	for(i=0; i<CANTIDAD_DIGITOS;i++)
	{
		DigitoABit(datos[i]);
		GPIOOn(pines[6-i]);
		GPIOOff(pines[6-i]);
	}
	return true;
}

/*Función para leer valor y retornarlo*/
uint16_t ITSE0803ReadValue(void)
{
	return valor_numerico;

}

/*Función para desinicializar*/
bool ITSE0803Deinit(gpio_t * pins)
{
	void GPIODeinit();
	return true;
}


/*==================[end of file]============================================*/
