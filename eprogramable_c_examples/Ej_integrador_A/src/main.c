/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es: Lovatto, Flavio Alexis
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */




/*Realice un función que reciba un puntero a una estructura LED como la que se muestra a
continuación:
struct leds
{
uint8_t n_led; indica el número de led a controlar
uint8_t n_ciclos; indica la cantidad de cilcos de encendido/apagado
uint8_t periodo; indica el tiempo de cada ciclo
uint8_t mode; ON, OFF, TOGGLE
}my_leds,*/


/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t n_led;     		/* indica el número de led a controlar*/
	uint8_t n_ciclos; 		/* indica la cantiad de ciclos de encendido/apagado */
	uint8_t periodo; 		/* indica el tiempo de cada ciclo*/
	uint8_t mode;           /* indica el modo en que funciona on, off y toggle*/

} my_leds;

const int ON=1;
const int OFF=0;
const int TOGGLE=7;
const int retardo=100;
/*==================[internal functions declaration]=========================*/
void ControlDeLeds(my_leds *led)
{
	uint8_t i=0;
	uint8_t j=0;
	if(led->mode==ON)
	{	switch(led->n_led)
		{
			case 1:  printf(" Enciendo el LED 1 \r\n");	break;
			case 2:  printf(" Enciendo el LED 2 \r\n");	break;
			case 3:  printf(" Enciendo el LED 3 \r\n");	break;
			default:  printf(" No se logro encender ningun led \r\n");
		}
	}
	else
	{   if(led->mode==OFF)
		{
							switch(led->n_led)
							{
								case 1:  printf(" Apago el LED 1 \r\n");	break;
								case 2:  printf(" Apago el LED 2 \r\n");	break;
								case 3:  printf(" Apago el LED 3 \r\n");	break;
								default:  printf(" No se logro apagar ningun led \r\n");
							}

		}
			else
			{
				if(led->mode==TOGGLE)
				{
									while(i<led->n_ciclos)
									{
												switch(led->n_led)
												{
													case 1: printf(" TOGGLE LED 1 \r\n");	break;
													case 2: printf(" TOGGLE LED 2 \r\n");	break;
													case 3: printf(" TOGGLE LED 3 \r\n");	break;
												}
													i++;
													while(j<retardo)
													{
														j++;
													}

									}

				}
				else
				{
					printf(" FIN \r\n");
				}

			}
	}

}


int main(void)
{
	my_leds LED;
	LED.mode=7;
	LED.n_ciclos=10;
	LED.n_led=2;
	ControlDeLeds(&LED);

	return 0;
}

/*==================[end of file]============================================*/

