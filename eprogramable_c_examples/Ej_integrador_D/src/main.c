/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es: Lovatto, Flavio
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
gpioConf_t.
typedef struct
{
uint8_t port; !< GPIO port number
uint8_t pin; !< GPIO pin number
uint8_t dir; !< GPIO direction ‘0’ IN; ‘1’ OUT
gpioConf_t;
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14
La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector
anterior operar sobre el puerto y pin que corresponda.
V0.0 (04-2019 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t port;
	uint8_t pin;
	uint8_t dir;

} gpioConf_t;



/*==================[internal functions declaration]=========================*/
void Valores(gpioConf_t *valoresPuertoPines)
{
	valoresPuertoPines[0].port=0;
	valoresPuertoPines[0].pin=2;
	valoresPuertoPines[0].dir=1;
	valoresPuertoPines[1].port=1;
	valoresPuertoPines[1].pin=3;
	valoresPuertoPines[1].dir=0;
	valoresPuertoPines[2].port=2;
	valoresPuertoPines[2].pin=4;
	valoresPuertoPines[2].dir=1;
	valoresPuertoPines[3].port=1;
	valoresPuertoPines[3].pin=6;
	valoresPuertoPines[3].dir=0;
	valoresPuertoPines[4].port=2;
	valoresPuertoPines[4].pin=6;
	valoresPuertoPines[4].dir=0;
	valoresPuertoPines[5].port=0;
	valoresPuertoPines[5].pin=9;
	valoresPuertoPines[5].dir=0;
	valoresPuertoPines[6].port=3;
	valoresPuertoPines[6].pin=8;
	valoresPuertoPines[6].dir=1;
	valoresPuertoPines[7].port=3;
	valoresPuertoPines[7].pin=2;
	valoresPuertoPines[7].dir=0;
	valoresPuertoPines[8].port=4;
	valoresPuertoPines[8].pin=6;
	valoresPuertoPines[8].dir=1;
	valoresPuertoPines[9].port=4;
	valoresPuertoPines[9].pin=1;
	valoresPuertoPines[9].dir=0;
}


void ConfiguroPuertos(uint8_t bcd, gpioConf_t *pines)
{

	switch(bcd)
	{
		case 0: printf("%d PUERTO  \r\n", pines[0].port);
				printf("%d PIN \r\n", pines[0].pin);
				printf("%d ESTADO \r\n", pines[0].dir);
				break;
		case 1: printf("%d PUERTO  \r\n", pines[1].port);
				printf("%d PIN \r\n", pines[1].pin);
				printf("%d ESTADO \r\n", pines[1].dir);
				break;
		case 2: printf("%d PUERTO  \r\n", pines[2].port);
				printf("%d PIN \r\n", pines[2].pin);
				printf("%d ESTADO \r\n", pines[2].dir);
				break;
		case 3: printf("%d PUERTO  \r\n", pines[3].port);
				printf("%d PIN \r\n", pines[3].pin);
				printf("%d ESTADO \r\n", pines[3].dir);
				break;
		case 4: printf("%d PUERTO  \r\n", pines[4].port);
				printf("%d PIN \r\n", pines[4].pin);
				printf("%d ESTADO \r\n", pines[4].dir);
				break;
		case 5: printf("%d PUERTO  \r\n", pines[5].port);
				printf("%d PIN \r\n", pines[5].pin);
				printf("%d ESTADO \r\n", pines[5].dir);
				break;
		case 6: printf("%d PUERTO  \r\n", pines[6].port);
				printf("%d PIN \r\n", pines[6].pin);
				printf("%d ESTADO \r\n", pines[6].dir);
				break;
		case 7: printf("%d PUERTO  \r\n", pines[7].port);
				printf("%d PIN \r\n", pines[7].pin);
				printf("%d ESTADO \r\n", pines[7].dir);
				break;
		case 8: printf("%d PUERTO  \r\n", pines[8].port);
				printf("%d PIN \r\n", pines[8].pin);
				printf("%d ESTADO \r\n", pines[8].dir);
				break;
		case 9: printf("%d PUERTO  \r\n", pines[9].port);
				printf("%d PIN \r\n", pines[9].pin);
				printf("%d ESTADO \r\n", pines[9].dir);
				break;
	}


}



int main(void)
{
	uint8_t BCD=7;
	gpioConf_t puertos[10];
	Valores (puertos);
	ConfiguroPuertos(BCD, puertos);

	return 0;
}

/*==================[end of file]============================================*/

