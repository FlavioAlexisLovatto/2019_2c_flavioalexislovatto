/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2019
 * Autor/es: Lovatto, Flavio Alexis
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
caracteres y edad.
a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
b. Defina un puntero a esa estructura y cargue los campos con los datos de su
compañero (usando acceso por punteros).*/

struct alumno {
				char nombres[12];
				char apellidos[20];
				int edad;
			  };

int main(void)
{
	struct alumno al1;
	char aux_nom[]="franco";
	char aux_ape[]="sanchez";

    strcpy(al1.nombres,aux_nom);
    strcpy(al1.apellidos,aux_ape);
    al1.edad=25;
    printf("%s al1.nombres \r\n", al1.nombres);
    printf("%s al1.apellidos \r\n", al1.apellidos);
    printf("%d al1.edad \r\n", al1.edad);

    struct alumno *al2;
    char aux_nom2[]="flavio";
    char aux_ape2[]="lovatto";
    strcpy(al2->nombres,aux_nom2);
    printf("%s al2.nombres \r\n", al2->nombres);
    strcpy(al2->apellidos,aux_ape2);
    printf("%s al2.apellidoss \r\n", al2->apellidos);
    al2->edad=28;
    printf("%d al2.edad \r\n", al2->edad);
	return 0;
}

/*==================[end of file]============================================*/

